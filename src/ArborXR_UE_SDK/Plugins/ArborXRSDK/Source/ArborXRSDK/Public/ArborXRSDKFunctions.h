// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ArborXRSDKService.h"
#include "ArborXRSDKFunctions.generated.h"

UENUM(BlueprintType)
enum class EFunctionsExecOut : uint8
{
	OnSuccess,
	OnFailure
};

UCLASS(NotBlueprintable)
class UArborXRSDKFunctions : public UObject {
	GENERATED_BODY()

public:
	// service functions
	/** Initializes the ArborXR SDK and returns a service object to query the SDK. */
	UFUNCTION(BlueprintCallable, meta = (Keywords = "ArborXR ", DisplayName = "Initialize Service", ExpandEnumAsExecs = "Branches"), Category = "ArborXR")
		static void InitService(EFunctionsExecOut& Branches, UArborXRSDKService*& Service, FString& Error);
	/** Release ArborXR SDK service and removes the service object. */
	UFUNCTION(BlueprintCallable, meta = (Keywords = "ArborXR ", DisplayName = "Release Service", ExpandEnumAsExecs = "Branches"), Category = "ArborXR")
		static UPARAM(DisplayName = "Error") FString ReleaseService(UArborXRSDKService* Service, EServiceExecOut& Branches);
};
