// Copyright Epic Games, Inc. All Rights Reserved.

using System.IO;
using UnrealBuildTool;

public class ArborXRSDK : ModuleRules
{
	public ArborXRSDK(ReadOnlyTargetRules Target) : base(Target)
	{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[] { });
        PrivateIncludePaths.AddRange(new string[] { });
        PrivateIncludePathModuleNames.AddRange(new[] { "Settings" });
        PublicDependencyModuleNames.AddRange(new[] { "Core", "CoreUObject", "Engine", "InputCore", "Projects" });
        PrivateDependencyModuleNames.AddRange(new string[] { });
        DynamicallyLoadedModuleNames.AddRange(new string[] { });

        if (Target.Platform != UnrealTargetPlatform.Android) return;

        var bHasArbor = false;
        var arborSdkDir = "";
        try
        {
            arborSdkDir = Path.Combine(ModuleDirectory, "../ThirdParty");
            bHasArbor = Directory.Exists(arborSdkDir);
        }
        catch (System.Exception)
        {
            // ignored
        }

        PublicIncludePathModuleNames.Add("Launch");

        if (bHasArbor)
        {
            var msg = string.Format("Arbor SDK found in {0}", arborSdkDir);
            System.Console.WriteLine(msg);
            PublicAdditionalLibraries.Add(arborSdkDir);

            PrivateDependencyModuleNames.Add("Launch");

            var pluginPath = Utils.MakePathRelativeTo(ModuleDirectory, Target.RelativeEnginePath);
            AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(pluginPath, "ArborXRSDK_UPL_Android.xml"));
        }
        else
        {
            var err = string.Format("Arbor SDK not found in {0}", arborSdkDir);
            System.Console.WriteLine(err);
        }
    }
}
