// Fill out your copyright notice in the Description page of Project Settings.


#include "ArborXRSDKService.h"

bool UArborXRSDKService::IsServiceConnected()
{
#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			UE_LOG(LogTemp, Error, TEXT("[ArborXR SDK] (IsServiceConnected) No GameActivity class found."));

			return false;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_IsServiceConnected", "()Z", false);

		if (!MethodId)
		{
			UE_LOG(LogTemp, Error, TEXT("[ArborXR SDK] Unable to find \"IsServiceConnected\" Java method in GameActivity class."));

			return false;
		}

		bool isServiceConnected = FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		return isServiceConnected;
	}
#endif

	UE_LOG(LogTemp, Warning, TEXT("[ArborXR SDK] (IsServiceConnected) No Android Java environment found."));

	return false;
}

void UArborXRSDKService::GetDeviceId(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving device id."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetDeviceId) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetDeviceId", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetDeviceId\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve device id.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetDeviceId) No Android Java environment found.");

	UE_LOG(LogTemp, Warning, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetDeviceTitle(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving device title."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetDeviceTitle) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetDeviceTitle", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetDeviceTitle\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve device title.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetDeviceTitle) No Android Java environment found.");

	UE_LOG(LogTemp, Warning, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetDeviceSerial(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving device serial."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetDeviceSerial) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetDeviceSerial", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetDeviceSerial\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve device serial.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetDeviceSerial) No Android Java environment found.");

	UE_LOG(LogTemp, Warning, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetDeviceTags(EServiceExecOut& Branches, TArray<FString>& Tags, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving device tags."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetDeviceTags) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetDeviceTags", "()[Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetDeviceTags\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jobjectArray MethodResult = (jobjectArray)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve device id.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jsize ArrayLength = Env->GetArrayLength(MethodResult);

		for (int i = 0; i < ArrayLength; i++) {
			jstring MethodResultItem = (jstring)Env->GetObjectArrayElement(MethodResult, i);

			const char* MethodResultChars = Env->GetStringUTFChars(MethodResultItem, 0);
			Env->ReleaseStringUTFChars(MethodResultItem, MethodResultChars);

			Tags.Add(FString(MethodResultChars));
		}

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetDeviceTags) No Android Java environment found.");

	UE_LOG(LogTemp, Warning, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;

	return;
}

void UArborXRSDKService::GetOrgId(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving organization id."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetOrgId) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetOrgId", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetOrgId\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve organization id.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetOrgId) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetOrgTitle(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving organization title."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetOrgTitle) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetOrgTitle", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetOrgTitle\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve organization title.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetOrgTitle) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetOrgSlug(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving organization slug."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetOrgSlug) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetOrgSlug", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetOrgSlug\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve organization slug.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetOrgSlug) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetMacAddressFixed(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving fixed mac address."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetMacAddressFixed) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetMacAddressFixed", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetMacAddressFixed\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve fixed mac address.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetMacAddressFixed) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetMacAddressRandom(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving random mac address."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetMacAddressRandom) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetMacAddressRandom", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetMacAddressRandom\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve random mac address.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetMacAddressRandom) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::TrackUserExperience(FUserExperience Experience, EServiceExecOut& Branches, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Tracking user experience."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (TrackUserExperience) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_TrackUserExperience", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"TrackUserExperience\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, MethodId, Env->NewStringUTF(TCHAR_TO_UTF8(*Experience.Actor)), Env->NewStringUTF(TCHAR_TO_UTF8(*Experience.Verb)), Env->NewStringUTF(TCHAR_TO_UTF8(*Experience.Object)));

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (TrackUserExperience) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetIsAuthenticated(EServiceExecOut& Branches, bool& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving authentication status."));

	Result = false;

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetIsAuthenticated) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetIsAuthenticated", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetIsAuthenticated\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve authentication status.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		Result = FString(MethodResultChars) == "true";

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetIsAuthenticated) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetAccessToken(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving access token."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetAccessToken) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetAccessToken", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetAccessToken\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve access token.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetAccessToken) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetRefreshToken(EServiceExecOut& Branches, FString& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving refresh token."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetRefreshToken) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetRefreshToken", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetRefreshToken\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve refresh token.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		Result = FString(MethodResultChars);

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetRefreshToken) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}

void UArborXRSDKService::GetExpiresDateUtc(EServiceExecOut& Branches, FDateTime& Result, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Retrieving expiry date in UTC."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (GetExpiresDateUtc) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_GetExpiresDateUtc", "()Ljava/lang/String;", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"GetExpiresDateUtc\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		jstring MethodResult = (jstring)FJavaWrapper::CallObjectMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (!MethodResult)
		{
			Error = TEXT("[ArborXR SDK] Unable to retrieve expiry date in UTC.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return;
		}

		const char* MethodResultChars = Env->GetStringUTFChars(MethodResult, 0);
		Env->ReleaseStringUTFChars(MethodResult, MethodResultChars);

		FDateTime DateTime;
		FDateTime::Parse(FString(MethodResultChars), DateTime);
		Result = DateTime;

		Branches = EServiceExecOut::OnSuccess;

		return;
	}
#endif

	Error = TEXT("[ArborXR SDK] (GetExpiresDateUtc) No Android Java environment found.");

	UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;
}
