// Copyright Epic Games, Inc. All Rights Reserved.

#include "ArborXRSDK.h"
#include "ArborXRSDKFunctions.h"

#define LOCTEXT_NAMESPACE "FArborXRSDKModule"

void FArborXRSDKModule::StartupModule()
{
}

void FArborXRSDKModule::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FArborXRSDKModule, ArborXRSDK)