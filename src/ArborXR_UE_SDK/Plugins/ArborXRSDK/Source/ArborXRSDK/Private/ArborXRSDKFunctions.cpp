#include "ArborXRSDKFunctions.h"

#if PLATFORM_ANDROID
#include "Android/AndroidApplication.h"
#include "Android/AndroidJNI.h"
#include "Android/AndroidJava.h"
#endif

void UArborXRSDKFunctions::InitService(EFunctionsExecOut& Branches, UArborXRSDKService*& Service, FString& Error)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Initializing service."));

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (InitService) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EFunctionsExecOut::OnFailure;

			return;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_InitService", "()Z", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"InitService\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EFunctionsExecOut::OnFailure;

			return;
		}

		bool MethodResult = FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		if (MethodResult) {
			UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Service initialized."));

			Service = NewObject<UArborXRSDKService>();

			Branches = EFunctionsExecOut::OnSuccess;

			return;
		} else
		{
			Error = TEXT("[ArborXR SDK] Could not initialize service.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EFunctionsExecOut::OnFailure;

			return;
		}
	}
#endif

	Error = TEXT("[ArborXR SDK] (InitService) No Android Java environment found.");

	UE_LOG(LogTemp, Warning, TEXT("%s"), *Error);

	Branches = EFunctionsExecOut::OnFailure;
}

FString UArborXRSDKFunctions::ReleaseService(UArborXRSDKService* Service, EServiceExecOut& Branches)
{
	UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Releasing service."));

	FString Error;

#if PLATFORM_ANDROID
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv(true))
	{
		if (!FJavaWrapper::GameActivityClassID)
		{
			Error = TEXT("[ArborXR SDK] (ReleaseService) No GameActivity class found.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return Error;
		}

		jmethodID MethodId = FJavaWrapper::FindMethod(Env, FJavaWrapper::GameActivityClassID, "ArborXR_ReleaseService", "()V", false);

		if (!MethodId)
		{
			Error = TEXT("[ArborXR SDK] Unable to find \"ReleaseService\" Java method in GameActivity class.");

			UE_LOG(LogTemp, Error, TEXT("%s"), *Error);

			Branches = EServiceExecOut::OnFailure;

			return Error;
		}

		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, MethodId);

		UE_LOG(LogTemp, Display, TEXT("[ArborXR SDK] Service released."));

		Service = nullptr;

		Branches = EServiceExecOut::OnSuccess;

		return FString();
	}
#endif

	Error = TEXT("[ArborXR SDK] (ReleaseService) No Android Java environment found.");

	UE_LOG(LogTemp, Warning, TEXT("%s"), *Error);

	Branches = EServiceExecOut::OnFailure;

	return Error;
}
