// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ArborXR_UE_SDKGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ARBORXR_UE_SDK_API AArborXR_UE_SDKGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
