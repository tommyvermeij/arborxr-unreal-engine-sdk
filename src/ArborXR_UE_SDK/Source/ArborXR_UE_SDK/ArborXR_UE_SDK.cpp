// Copyright Epic Games, Inc. All Rights Reserved.

#include "ArborXR_UE_SDK.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ArborXR_UE_SDK, "ArborXR_UE_SDK" );
