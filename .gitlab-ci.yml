workflow:
  rules:
    - if: $CI_COMMIT_TAG

stages:
    - build
    - deploy

build-job:
    stage: build
    image: alpine
    before_script:
        - apk add --update zip
    script:
        - |
            cd "src/ArborXR_UE_SDK/Plugins"
            zip -r "${CI_PROJECT_DIR}/package.zip" ArborXRSDK
            cd "${CI_PROJECT_DIR}"
    artifacts:
        paths:
            - ./package.zip
        when: on_success

deploy-job:
    stage: deploy
    image: alpine
    dependencies:
        - build-job
    before_script:
        - apk add --update curl jq
    script:
        - |
            # Define variables for GitLab API
            GITLAB_API="https://gitlab.com/api/v4"
            PROJECT_ID="${CI_PROJECT_ID}"
            TAG_NAME="${CI_COMMIT_TAG}"
            TAG_MESSAGE="${CI_COMMIT_TAG_MESSAGE}"
            TOKEN="${CI_JOB_TOKEN}"

            # Check if TAG_NAME is set
            if [ -z "$TAG_NAME" ]; then
                echo "TAG_NAME is not set. Cannot create a release without a tag."
                exit 1
            fi

            # Check if TAG_MESSAGE is set
            if [ -z "$TAG_MESSAGE" ]; then
                RELEASE_NOTES="${TAG_NAME}"
            else
                RELEASE_NOTES="${TAG_MESSAGE}"
            fi

            data=$(jq -n --arg name "$TAG_NAME" --arg desc "$RELEASE_NOTES" \
                    '{
                        "name": $name,
                        "tag_name": $name,
                        "description": $desc
                    }')

            # Create a release
            curl --request POST --header "JOB-TOKEN: ${TOKEN}" --header "Content-Type: application/json" --data "$data" "${GITLAB_API}/projects/${PROJECT_ID}/releases"

            # Upload plugin zip file to project
            response=$(curl --request PUT \
                            --header "JOB-TOKEN: ${TOKEN}" \
                            --upload-file "package.zip" \
                            "${GITLAB_API}/projects/${PROJECT_ID}/packages/generic/plugins/${TAG_NAME}/ArborXR_SDK_${TAG_NAME}.zip")

            # Use the extracted URL to attach the uploaded file to the release
            attach_response=$(curl --request POST \
                                    --header "JOB-TOKEN: ${TOKEN}" \
                                    --data "name=ArborXR_SDK_${TAG_NAME}.zip" \
                                    --data "link_type=package" \
                                    --data "url=${GITLAB_API}/projects/${PROJECT_ID}/packages/generic/plugins/${TAG_NAME}/ArborXR_SDK_${TAG_NAME}.zip" \
                                    "${GITLAB_API}/projects/${PROJECT_ID}/releases/${TAG_NAME}/assets/links")

            # Check if the attachment was successful
            if echo $attach_response | jq . &> /dev/null; then
                echo "ArborXR_SDK_${TAG_NAME}.zip has been attached to the release."
            else
                echo "Error: Failed to attach ArborXR_SDK_${TAG_NAME}.zip to the release."
                echo "Response: ${attach_response}"
                exit 1
            fi
